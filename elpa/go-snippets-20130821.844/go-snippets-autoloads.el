;;; go-snippets-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (go-snippets-initialize) "go-snippets" "go-snippets.el"
;;;;;;  (21093 5666))
;;; Generated autoloads from go-snippets.el

(autoload 'go-snippets-initialize "go-snippets" "\


\(fn)" nil nil)

(eval-after-load 'yasnippet '(go-snippets-initialize))

;;;***

;;;### (autoloads nil nil ("go-snippets-pkg.el") (21093 5666 521828))

;;;***

(provide 'go-snippets-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; go-snippets-autoloads.el ends here
